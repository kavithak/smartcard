package com.smartcard.api.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import com.smartcard.api.dao.EmployeeDao;
import com.smartcard.api.entity.Employee;
import com.smartcard.api.exception.EmployeeAlreadyExistException;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class AdminServiceImplTest {

	@InjectMocks
	private AdminServiceImpl adminServiceImplMock;

	@Mock
	private EmployeeDao employeeMock;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllEmployeesTest() {
		Employee employee = new Employee("aa", "bb", "aa.bb@gmail.com");

		List<Employee> empList = new ArrayList<Employee>();
		empList.add(employee);
		Iterable<Employee> empIterble = empList;

		Mockito.when(employeeMock.findAll()).thenReturn(empIterble);

		List<Employee> retrivedList = adminServiceImplMock.getAllEmployees();

		assertEquals(empList, retrivedList);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void createEmployeeNewEmployeeTest() {
		Employee employee = new Employee("aa", "bb", "newmail@gmail.com");

		Mockito.when(employeeMock.findByEmailId("newmail@gmail.com")).thenReturn(null);

		adminServiceImplMock.createEmployee(employee);

		verify(employeeMock, times(1)).save(any(Employee.class));

	}

	@Test
	public void createEmployeeAlreadyExistingTest() {
		Employee employee = new Employee("aa", "bb", "newmail@gmail.com");

		Mockito.when(employeeMock.findByEmailId("newmail@gmail.com")).thenReturn(employee);

		Exception exception = assertThrows(EmployeeAlreadyExistException.class,
				() -> adminServiceImplMock.createEmployee(employee));

		assertTrue(exception.getMessage().contains("Employee already exists with the given email id"));

	}

	@Test
	public void getEmployeeInfoTest() {
		Employee employee = new Employee("aa", "bb", "newmail@gmail.com");

		Mockito.when(employeeMock.findById(1)).thenReturn(Optional.of(employee));

		Employee retrivedemployee = adminServiceImplMock.getEmployeeInfo(1);

		assertSame(employee, retrivedemployee);
	}
}
