package com.smartcard.api.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.smartcard.api.dao.EmployeeDao;
import com.smartcard.api.dao.EmployeeWorkingHoursDao;
import com.smartcard.api.entity.Employee;
import com.smartcard.api.entity.EmployeeWorkingHours;
import com.smartcard.api.exception.NoCheckInFoundException;
import com.smartcard.api.exception.NoEmployeeFoundException;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTest {

	@InjectMocks
	private EmployeeServiceImpl employeeServiceImplMock;

	@Mock
	private EmployeeDao employeeDaoMock;

	@Mock
	private EmployeeWorkingHoursDao workingHoursDaoMock;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void checkInTest() {
		LocalDateTime localDateTime = LocalDateTime.now();
		EmployeeWorkingHours workingHours = new EmployeeWorkingHours();

		workingHours.setDay(localDateTime.toLocalDate());
		workingHours.setCheckIn(localDateTime.toLocalTime());

		Employee employee = new Employee("aa", "bb", "aa.bb@gmail.com");

		Mockito.when(employeeDaoMock.findById(1)).thenReturn(Optional.of(employee));

		employeeServiceImplMock.checkIn(1);

		verify(workingHoursDaoMock, times(1)).save(any(EmployeeWorkingHours.class));
	}

	@Test
	public void checkInTestExceptionTest() {

		Mockito.when(employeeDaoMock.findById(1)).thenReturn(Optional.empty());

		Exception exception = assertThrows(NoEmployeeFoundException.class, () -> employeeServiceImplMock.checkIn(1));
		
		assertTrue(exception.getMessage().contains("Employee id does not exist"));

	}

	@Test
	public void checkOutTest() throws NoCheckInFoundException {
		EmployeeWorkingHours employeeWorkingHours = new EmployeeWorkingHours();
		
		Mockito.when(workingHoursDaoMock.checkCheckInAlreadyExists(1, LocalDate.now()))
				.thenReturn(employeeWorkingHours);
		
		employeeServiceImplMock.checkOut(1);
		
		verify(workingHoursDaoMock, times(1)).save(any(EmployeeWorkingHours.class));
		
	}
	
	
	
	@Test
	public void checkOutExceptionTest()  {
		
		Mockito.when(workingHoursDaoMock.checkCheckInAlreadyExists(1, LocalDate.now())).thenReturn(null);
		
		Exception exception = assertThrows(
				NoCheckInFoundException.class, 
				() -> employeeServiceImplMock.checkOut(1));
		
		assertTrue(exception.getMessage().contains("No check in record  found"));
	}
	
}
