package com.smartcard.api.mail;

import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.smartcard.api.dao.PartTimeEmployeeDao;
import com.smartcard.api.dao.RegularEmployeeDao;
import com.smartcard.api.entity.Employee;

@Service
public class SendMail {

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private PartTimeEmployeeDao partTimeEmployeeDao;
	
	@Autowired
	private RegularEmployeeDao regularEmployeeDao;

	public void sendEmailToPartTimeEmployees() throws Exception {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		List<Employee> partTimeEmployees = partTimeEmployeeDao.findAllAttendenceShortage();
		for (Employee partTimeEmployee : partTimeEmployees) {
			helper.setTo(partTimeEmployee.getEmail());
			helper.setText("You have shortage of working hours, Please contact the Admin");
			helper.setSubject("Shortage of Working Hours");
			sender.send(message);
		}
	}

	
	public void sendEmailToRegularEmployees() throws Exception {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		List<Employee> regularEmployees = regularEmployeeDao.findAllAttendenceShortage();
		for (Employee regularEmployee : regularEmployees) {
			helper.setTo(regularEmployee.getEmail());
			helper.setText("You have shortage of working hours, Please contact the Admin");
			helper.setSubject("Shortage of Working Hours");
			sender.send(message);
		}
	}
}
