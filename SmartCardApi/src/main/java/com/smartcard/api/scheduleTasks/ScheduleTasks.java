package com.smartcard.api.scheduleTasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.smartcard.api.dao.EmployeeWorkingHoursDao;
import com.smartcard.api.mail.SendMail;

@Component
public class ScheduleTasks {

	@Autowired
	private SendMail sendMail;
	
	@Autowired
	private EmployeeWorkingHoursDao employeeWorkingHoursDao;

	@Scheduled(cron = "${cron.email}")
	public void scheduleMailSendingTask_PartTime() throws Exception {
		sendMail.sendEmailToRegularEmployees();
	}
	
	@Scheduled(cron = "${cron.email}")
	public void scheduleMailSendingTask_RegularEmployee() throws Exception {
		sendMail.sendEmailToRegularEmployees();
	}
	
	
	@Scheduled(cron = "${cron.delete}")
	public void scheduleDeletingPreviousEntries() throws Exception {
		employeeWorkingHoursDao.deleteAll();
	}
}
