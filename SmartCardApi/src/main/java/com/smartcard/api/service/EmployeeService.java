package com.smartcard.api.service;

import com.smartcard.api.exception.NoCheckInFoundException;
import com.smartcard.api.exception.NoEmployeeFoundException;

public interface EmployeeService {
	
	public void checkIn(int id) throws NoEmployeeFoundException;
	
	public void checkOut(int id) throws NoCheckInFoundException;
}
