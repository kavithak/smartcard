package com.smartcard.api.service;

import java.util.List;

import com.smartcard.api.entity.Employee;
import com.smartcard.api.exception.EmployeeAlreadyExistException;
import com.smartcard.api.exception.NoEmployeeFoundException;

public interface AdminService {

	public List<Employee> getAllEmployees();
	
	public void createEmployee(Employee employee) throws EmployeeAlreadyExistException;
	
	public Employee getEmployeeInfo(int id) throws NoEmployeeFoundException;
	
	public void deleteEmployee(int id) throws NoEmployeeFoundException;
	
    public void modifyExistingEmployeeData(int id, Employee employee) ;
	
}
