package com.smartcard.api.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartcard.api.dao.EmployeeDao;
import com.smartcard.api.entity.Employee;
import com.smartcard.api.entity.RegularEmployee;
import com.smartcard.api.exception.EmployeeAlreadyExistException;
import com.smartcard.api.exception.NoEmployeeFoundException;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private EmployeeDao<Employee> employeeDao;

	@Override
	public List<Employee> getAllEmployees() {

		List<Employee> empList = new ArrayList<Employee>();
		Iterable<Employee> empIterble = employeeDao.findAll();
		Iterator<Employee> employeeIterator = empIterble.iterator();
		while (employeeIterator.hasNext()) {
			Employee employee = employeeIterator.next();
			if (employee instanceof RegularEmployee)
				employee.setIsRegularEmployee(true);
		}
		empIterble.forEach(empList::add);
		return empList;
	}

	@Override
	public void createEmployee(Employee employee) throws EmployeeAlreadyExistException{
		Employee existingEmployee = employeeDao.findByEmailId(employee.getEmail());
		if (existingEmployee != null) {
			throw new EmployeeAlreadyExistException(employee);
		} else
			employeeDao.save(employee);
	}

	@Override
	public Employee getEmployeeInfo(int id) throws NoEmployeeFoundException {
		Optional<Employee> optionalEmployee = employeeDao.findById(id);
		optionalEmployee.orElseThrow(() -> new NoEmployeeFoundException(id));
		return optionalEmployee.get();
	}

	@Override
	public void deleteEmployee(int id) throws NoEmployeeFoundException {
		Employee employee = getEmployeeInfo(id);
		if(employee != null)
		employeeDao.deleteById(id);
	}

	@Override
	public void modifyExistingEmployeeData(int id, Employee newEmployee)   {
		employeeDao.findById(id).ifPresentOrElse((emp) -> {
			emp.setFirstName(newEmployee.getFirstName());
			emp.setLastName(newEmployee.getLastName());
			emp.setEmail(newEmployee.getEmail());
		}, () -> {
			try {
				throw new NoEmployeeFoundException(id);
			} catch (NoEmployeeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

}
