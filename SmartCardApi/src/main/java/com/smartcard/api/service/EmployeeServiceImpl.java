package com.smartcard.api.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartcard.api.dao.EmployeeDao;
import com.smartcard.api.dao.EmployeeWorkingHoursDao;
import com.smartcard.api.entity.Employee;
import com.smartcard.api.entity.EmployeeWorkingHours;
import com.smartcard.api.exception.NoCheckInFoundException;
import com.smartcard.api.exception.NoEmployeeFoundException;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao<Employee> employeeDao;

	@Autowired
	private EmployeeWorkingHoursDao employeeWorkingHoursDao;


	@Override
	public void checkIn(int id) throws NoEmployeeFoundException {
		Optional<Employee> employee = employeeDao.findById(id);
		
		
		employee.ifPresentOrElse((emp) -> {
			LocalDate localDate = LocalDate.now();
			ZoneId zone = ZoneId.of("Europe/Berlin");
			LocalTime time = LocalTime.now(zone);
			EmployeeWorkingHours workingHours = new EmployeeWorkingHours();

			workingHours.setDay(localDate);
			workingHours.setCheckIn(time);
			workingHours.setEmployee(emp);

			employeeWorkingHoursDao.save(workingHours);
		}, () -> {
			throw new NoEmployeeFoundException(id);
		});
		
		
	}

	@Override
	public void checkOut(int id) throws NoCheckInFoundException {
		ZoneId zone = ZoneId.of("Europe/Berlin");
		LocalTime time = LocalTime.now(zone);
		EmployeeWorkingHours employeeWorkingHours = employeeWorkingHoursDao.checkCheckInAlreadyExists(id,
				LocalDate.now());
		if (employeeWorkingHours != null) {
			employeeWorkingHours.setCheckOut(time);
			employeeWorkingHoursDao.save(employeeWorkingHours);
		} else
			throw new NoCheckInFoundException(id);
	}

}
