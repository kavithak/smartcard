package com.smartcard.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartcard.api.entity.PartTimeEmployee;
import com.smartcard.api.entity.RegularEmployee;
import com.smartcard.api.exception.NoCheckInFoundException;
import com.smartcard.api.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	

	@PutMapping("/checkIn/{id}")
	public void checkIn(@PathVariable(value = "id") int id) {

		employeeService.checkIn(id);
	}

	@PutMapping("/checkOut/{id}")
	public void checkOut(@PathVariable(value = "id") int id) throws NoCheckInFoundException {
		employeeService.checkOut(id);
	}

	
	

	
	
	
}
