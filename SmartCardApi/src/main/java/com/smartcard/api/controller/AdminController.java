package com.smartcard.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartcard.api.entity.PartTimeEmployee;
import com.smartcard.api.entity.Employee;
import com.smartcard.api.entity.RegularEmployee;
import com.smartcard.api.exception.EmployeeAlreadyExistException;
import com.smartcard.api.exception.NoEmployeeFoundException;
import com.smartcard.api.service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController extends EmployeeController {

	@Autowired
	private AdminService adminService;
	
	
	@GetMapping("/allEmployees")
	public List<Employee> getAllEmployees() {
	List<Employee> employeesList = adminService.getAllEmployees();
		return employeesList;
	}
	
	@PostMapping("/addRegularEmployee")
	public void addRegularEmployee(@RequestBody RegularEmployee employee) throws EmployeeAlreadyExistException {
		adminService.createEmployee(employee);
	}
	
	@PostMapping("/addContractEmployee")
	public void addContractEmployee(@RequestBody PartTimeEmployee employee) throws EmployeeAlreadyExistException {
		adminService.createEmployee(employee);
	}
	
	@PutMapping("/modify/{id}")
	public void modifyEmployeeData(@PathVariable(value = "id") int id, @RequestBody Employee employee) {
		adminService.modifyExistingEmployeeData(id, employee);
	}

	@DeleteMapping("/delete/{id}")
	public void deleteEmployee(@PathVariable(value = "id") int id) throws NoEmployeeFoundException {
		adminService.deleteEmployee(id);
	}
}