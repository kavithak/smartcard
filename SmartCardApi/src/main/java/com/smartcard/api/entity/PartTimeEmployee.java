package com.smartcard.api.entity;

import javax.persistence.Entity;

@Entity
public class PartTimeEmployee  extends Employee{
	
	private int workingHoursPerDay;
	
	private int ratePerHour;

	public int getWorkingHoursPerDay() {
		return workingHoursPerDay;
	}

	public void setWorkingHoursPerDay(int workingHoursPerDay) {
		this.workingHoursPerDay = workingHoursPerDay;
	}

	public int getRatePerHour() {
		return ratePerHour;
	}

	public void setRatePerHour(int ratePerHour) {
		this.ratePerHour = ratePerHour;
	}

}
