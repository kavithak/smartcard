package com.smartcard.api.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="employee_working_hours")
public class EmployeeWorkingHours {
	
	@Id
    @Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
 
	
	@Column(name = "date", columnDefinition = "DATE")  
	private LocalDate day;
	
	@Column(name="check_in",  columnDefinition = "TIME")
	private LocalTime checkIn;
	
	@Column(name = "check_out", columnDefinition = "TIME")
	private LocalTime checkOut;
	

	@ManyToOne
	@JsonIgnore
	private Employee employee;
	

	public void setDay(LocalDate day) {
		this.day = day;
	}


	public LocalDate getDay() {
		return day;
	}
	public LocalTime getCheckIn() {
		return checkIn;
	}


	public void setCheckIn( LocalTime checkIn) {
		this.checkIn = checkIn;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public LocalTime getCheckOut() {
		return checkOut;
	}


	@Override
	public String toString() {
		return "EmployeeWorkingHours [checkIn=" + checkIn + ", checkOut=" + checkOut + ", employee=" + employee + "]";
	}


	public void setCheckOut(LocalTime checkOut) {
		this.checkOut = checkOut;
	}


}
