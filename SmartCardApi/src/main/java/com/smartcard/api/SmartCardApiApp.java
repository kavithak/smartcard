package com.smartcard.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SmartCardApiApp {

	public static void main(String[] args) {
		SpringApplication.run(SmartCardApiApp.class, args);
	}

}
