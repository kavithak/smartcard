package com.smartcard.api.exception;

public class NoCheckInFoundException extends Exception {

	public  NoCheckInFoundException(int id) {
		super("No check in record  found, have to do check in ");
	}
}
