package com.smartcard.api.exception;

public class NoEmployeeFoundException extends RuntimeException {

	/**
	 * serial version id
	 */
	private static final long serialVersionUID = 1L;

	public NoEmployeeFoundException(int id) {
		super("Employee id does not exist");
	}
}
