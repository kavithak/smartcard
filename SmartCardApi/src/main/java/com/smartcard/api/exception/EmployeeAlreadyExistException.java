package com.smartcard.api.exception;

import com.smartcard.api.entity.Employee;

public class EmployeeAlreadyExistException extends RuntimeException {
	
	public EmployeeAlreadyExistException(Employee employee) {
		super("Employee already exists with the given email id");
	}

}
