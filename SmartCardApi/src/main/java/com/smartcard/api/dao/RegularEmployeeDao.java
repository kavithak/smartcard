 package com.smartcard.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.smartcard.api.entity.Employee;
import com.smartcard.api.entity.RegularEmployee;

public interface RegularEmployeeDao extends EmployeeDao<RegularEmployee> {

	@Query("select emp from EmployeeWorkingHours whours join whours.employee emp where (whours.checkOut - whours.checkIn) < 8")
	public List<Employee> findAllAttendenceShortage();
}
