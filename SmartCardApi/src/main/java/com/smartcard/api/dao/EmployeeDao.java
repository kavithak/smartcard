package com.smartcard.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.smartcard.api.entity.Employee;

public interface EmployeeDao<T extends Employee> extends CrudRepository<T, Integer> {
	
	@Query("Select e from Employee e where e.email = :emailId")
	Employee findByEmailId(String emailId);
	
//	public List<Employee> findAllAttendenceShortage();
	

}
