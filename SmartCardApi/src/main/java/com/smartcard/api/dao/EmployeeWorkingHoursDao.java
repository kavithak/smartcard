package com.smartcard.api.dao;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.smartcard.api.entity.EmployeeWorkingHours;

public interface EmployeeWorkingHoursDao extends CrudRepository<EmployeeWorkingHours, Integer> {
	
	@Query("Select ewh from EmployeeWorkingHours ewh where ewh.employee.id = :id and ewh.day = :date")
	EmployeeWorkingHours checkCheckInAlreadyExists(int id, LocalDate date);

}
