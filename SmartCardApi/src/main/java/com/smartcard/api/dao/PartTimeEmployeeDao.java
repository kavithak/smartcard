package com.smartcard.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.smartcard.api.entity.Employee;
import com.smartcard.api.entity.PartTimeEmployee;

public interface PartTimeEmployeeDao extends EmployeeDao<PartTimeEmployee> {
	
	@Query("select cemp from EmployeeWorkingHours whours join whours.employee cemp where (whours.checkOut - whours.checkIn) < cemp.workingHoursPerDay")
	public List<Employee> findAllAttendenceShortage();

}
