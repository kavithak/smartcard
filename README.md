Smart Card
--------------

An attendence api to log the working hours of the employees. 

Requirements-(Work in Progress, not Finalized)
----------------------
a) Roles of normal Employee
   
1) Login and logout time are passed and stored in the database of each employee
2) Each employee can view his working hours of a particular period (yet to implement)
3) Employee receives  an email when he has less working hours
4) Appropriate excception should be thrown whenever an issue occurs


b) Roles of Admin  employee

1) Admin  has all the employee prieveleges mentioned above
2) can create and delete the employees
3) Need to implement security(yet to implement)
4) Admin should be able to create employees and can see each employees working hours

c)Schedular

1)A cron job(mail_cronjob) has set for sending mail to the employee if he has a shortage of working hours in the previous day
2)Another cron job(delete_cronjob) has set for deleting previous employees attendence which runs only on sunday midnight.

Technology
-----------------------
Spring boot, Java 8, embedded DB(H2) / Derby , JUnit, Hibernate, REST, Mockito
Spring Security, swagger

How to build
-----------------------
Build using maven

How to run
-----------------------
1) Package with maven and run the App:

java -jar SmartCardApiApp.0.0.1-SNAPSHOT.jar

2) Use a REST client (I use postman, should have write permission at home dir)

create schema EmployeeInfo

3) ...

Features
----------------------

